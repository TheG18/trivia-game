Template.search.helpers({
    friended: function () {
        if(Meteor.user().profile) return Meteor.users().profile.friends.idexOf(this._id) + 1;
    },
    isYou: function () {
        return.Meteor.userId() === this._id;
    },
    usersindex: () => usersIndex
});

Template.search.events({
    "click .add-friend": function () {
        Meteor.users.update(Meteor.usersId(), {
            $addToSet: {
                "profile.friends": this._id
            }
        });
    }
});